package com.dosideas.camel.demo;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpoints;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest
@MockEndpoints
public class RouteParaTestTest {

    @Produce("direct:exportacionCsv")
    private ProducerTemplate producerTemplate;

    @Test
    @DirtiesContext
    public void contextLoads() throws InterruptedException {
        String body = "hola, mundo!";

//        for (int i = 0; i < 100; i++) {
//            producerTemplate.sendBody(body);
//        }
        producerTemplate.sendBody(body);

    }

}
