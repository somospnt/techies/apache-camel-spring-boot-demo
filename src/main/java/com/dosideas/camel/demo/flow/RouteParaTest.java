package com.dosideas.camel.demo.flow;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/** Este Route está creado para mostrar el testing de rutas 
 */
@Component
public class RouteParaTest extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:holaMundoTest")
                .log("${body}")
                .to("log:out");
    }
    
}
