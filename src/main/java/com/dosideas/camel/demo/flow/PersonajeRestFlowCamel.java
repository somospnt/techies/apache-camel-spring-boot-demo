package com.dosideas.camel.demo.flow;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class PersonajeRestFlowCamel extends RouteBuilder {


    @Override
    public void configure() throws Exception {
        restConfiguration().port(8080).bindingMode(RestBindingMode.json);

        rest("/personaje")
                .post()
                .consumes("application/json")
                .produces("text/plain")
                .to("direct:insertar-sql");

        from("direct:insertar-sql")
                .to("sql:insert into personaje (nombre, serie ) values (:#${body[nombre]}, :#${body[serie]})")
                .to("direct:mostrar");

        from("direct:mostrar")
                .setBody(constant("select * from personaje"))
                .to("jdbc:dataSource")
                .to("log:out");

    }


}
