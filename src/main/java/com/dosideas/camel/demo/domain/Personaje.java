package com.dosideas.camel.demo.domain;

public class Personaje {
    private Integer id;
    private String nombre;
    private String serie;

    public Personaje(Integer id, String nombre, String serie) {
        this.id = id;
        this.nombre = nombre;
        this.serie = serie;
    }

    public Personaje() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }
}
