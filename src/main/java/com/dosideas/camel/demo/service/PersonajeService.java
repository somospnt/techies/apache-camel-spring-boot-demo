package com.dosideas.camel.demo.service;

import org.springframework.stereotype.Component;

@Component
public class PersonajeService {
    
    public String aMayusculas(String body) {
        return body.toUpperCase();
    }
    
    public void metodoQueFalla() {
        throw new UnsupportedOperationException("Este método falla siempre");
    }
    
    public void soloPares(long id) {
        if (id % 2 != 0) {
            throw new IllegalArgumentException("Es impar, solo se aceptan pares");
        }
    }
    
}
