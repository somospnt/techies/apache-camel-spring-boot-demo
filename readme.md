# Apache Camel con Spring Boot
##### Una demo con ejemplos de integración

## Requisitos
JDK 8 o superior

## Estructura
El proyecto es un JAR ejecutable de Spring Boot. El pom incluye el starter de
Apache Camel, junto con la extensión camel-jdbc para acceso a datos. 

El proyecto hace uso de un DataSource en H2 para simular una base de datos
relacional. El archivo schema.sql contiene la tabla de ejemplo que se usa. 

La clase DemoRoute contiene varios ejemplos de rutas y ejecuciones varias. 
Algunas ejecuciones se inician con un mensaje directo, el cual es lanzado ni
bien termina de levantarse el contexto de Spring (en la clase ApplicationStartup).
